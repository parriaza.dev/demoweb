package com.parriaza;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import com.parriaza.repository.PersonaRepoImpl1;
import com.parriaza.service.IPersonaService;
import com.parriaza.service.IPersonaServiceImp;

@SpringBootApplication
public class DemoConsolaApplication implements CommandLineRunner{
	
	
	private static final Logger LOG = LoggerFactory.getLogger(DemoConsolaApplication.class);
	@Autowired
	private IPersonaService service;
	
	public static void main(String[] args) {
		SpringApplication.run(DemoConsolaApplication.class, args);
	}

	@Override
	public void run(String... args) throws Exception {
		// TODO Auto-generated method stub
//		System.out.println("Hola amiguitos");
//		LOG.info("Holi con el log");
//		LOG.warn("Holi con el log");
//		LOG.debug("Holi con el log");
//		LOG.error("Holi con el log");
		service = new IPersonaServiceImp();
		service.registrar("Parriaza");

	}
	
}
