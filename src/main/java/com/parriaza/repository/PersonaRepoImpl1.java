package com.parriaza.repository;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;

import lombok.extern.java.Log;
@Repository
@Qualifier("persona1")
public class PersonaRepoImpl1 implements IPersonaRepo{
	
	private static final Logger LOG = LoggerFactory.getLogger(PersonaRepoImpl1.class);

	@Override
	public void registrar(String nombre) {
		LOG.info("Se registro a: "+ nombre );
	}
}
