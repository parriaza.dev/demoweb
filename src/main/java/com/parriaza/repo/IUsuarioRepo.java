package com.parriaza.repo;

import org.springframework.data.jpa.repository.JpaRepository;

import com.parriaza.model.Persona;
import com.parriaza.model.Usuario;

public interface IUsuarioRepo extends JpaRepository<Usuario, Integer>{
	Usuario findByNombre(String nombre);
}
