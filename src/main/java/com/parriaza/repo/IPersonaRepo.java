package com.parriaza.repo;

import org.springframework.data.jpa.repository.JpaRepository;

import com.parriaza.model.Persona;

public interface IPersonaRepo extends JpaRepository<Persona, Integer>{

}
