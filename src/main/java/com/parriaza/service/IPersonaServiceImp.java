package com.parriaza.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Repository;

import com.parriaza.repository.IPersonaRepo;
import com.parriaza.repository.PersonaRepoImpl1;


@Repository

public class IPersonaServiceImp implements IPersonaService{
	@Autowired
	@Qualifier("persona2")
	private IPersonaRepo repo;
	
	@Override
	public void registrar(String nombre) {
		repo = new PersonaRepoImpl1();
		repo.registrar(nombre);
		
	}
}
