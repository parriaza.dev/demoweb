# Demo API Springboot 4 + Spring Data + Spring Web

Proyecto creado con motivos de estudio

Cuenta con la implementación de 2 disintas conecciones a base de datos que nos permite obtener los datos de la BD con JPA, utilizando la arquitectura que spring nos ofrece, esto en posterior, es desplegado en la plataforma de Heroku y publicado de manera inmediata.

[Link nodo en producción](https://parriaza-demo-boot.herokuapp.com/)

## Características
La API demo fue montado sobre framework de springboot STS 4 + Java 1.8.

Dependencias:
- Springboot Web
- mysql-runtime
- postgresql-runtime
Gestor de dependencia:
Maven
Bases de datos:
Mysql
Postgresql



## Motivación
Demostrar el conocimiento técnico requerido para montar una API sencilla que permita conocer fundamentos de la arquitectura de spring data y otros componentes

## Requerimientos
Para lanzar el proyecto se necesita:

+ [Springboot STS 4](https://spring.io/tools)
+ [Java 1.8](https://www.oracle.com/java/technologies/javase-jre8-downloads.html)
+ [Mysql](https://www.mysql.com/)
+ [Postgresql](https://www.postgresql.org/)


## Getting Started

	git clone https://gitlab.com/pabloarriazamillar/demoWeb.git
    Importar el proyecto a la intancia de Spring Tool Suite 4
    Run As -> Spring Boot App

## Licencia
license ([MIT](http://opensource.org/licenses/mit-license.php))
